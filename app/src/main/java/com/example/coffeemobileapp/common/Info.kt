package com.example.coffeemobileapp.common

import com.example.coffeemobileapp.common.Models.ModelMessage
import com.example.coffeemobileapp.common.Models.ModelUser

object Info {
    val listUsers: List<ModelUser> = arrayListOf(
        ModelUser("", "Sarah", "(123) 456-7890", "General"),
        ModelUser("", "John", "(123) 456-7890", "General"),
        ModelUser("", "Cindy", "(123) 456-7890", "General"),
        ModelUser("", "Shannon", "(123) 456-7890", "Recruitment specialist"),
        ModelUser("", "Josh", "(123) 456-7890", "Employment specialist"),
    )

    val listMessage: List<ModelMessage> = arrayListOf(
        ModelMessage(
            "Bring close-toed shoes to class tomorrow",
            "Tomorrow we will practice making different coffee recipes. It is important that you wear close-toed shoes. Please message me if you have any questions!",
            "09/07/21 7:00 AM",
            listUsers[0]
        ),
        ModelMessage(
            "Trivia night",
            "Looking for volunteers for 1951’s first trivia night! If you are able to to help, please reply to this message.",
            "06/07/21 5:00 AM",
            listUsers[2]
        ),
        ModelMessage(
            "Thank you for those who attended the open house",
            "Thank you for supporting the new class of students at yesterday’s open house event. The students really enjoyed meeting you and making coffee for you!",
            "03/07/21 6:00 AM",
            listUsers[1]
        ),
    )
}
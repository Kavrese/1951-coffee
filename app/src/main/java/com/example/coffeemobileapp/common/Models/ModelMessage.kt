package com.example.coffeemobileapp.common.Models

data class ModelMessage(
    val title: String,
    val message: String,
    val data: String,
    val user: ModelUser,
)

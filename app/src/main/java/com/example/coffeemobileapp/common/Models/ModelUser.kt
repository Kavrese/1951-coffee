package com.example.coffeemobileapp.common.Models

data class ModelUser(
    val src: String,
    val name: String,
    val phone: String,
    val type: String,
)

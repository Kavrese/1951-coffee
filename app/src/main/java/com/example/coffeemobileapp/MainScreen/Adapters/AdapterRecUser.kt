package com.example.coffeemobileapp.MainScreen.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.coffeemobileapp.common.Models.ModelMessage
import com.example.coffeemobileapp.R
import com.example.coffeemobileapp.common.Models.ModelUser

class AdapterRecUser(val listUser: List<ModelUser>): RecyclerView.Adapter<AdapterRecUser.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val src = itemView.findViewById<ImageView>(R.id.src_profile)
        val name = itemView.findViewById<TextView>(R.id.profile_name)
        val phone = itemView.findViewById<TextView>(R.id.profile_phone)
        val card = itemView.findViewById<CardView>(R.id.card_message_profile)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.card.setOnClickListener {

        }
        holder.name.text = listUser[position].name
        holder.phone.text = listUser[position].phone
    }

    override fun getItemCount(): Int = listUser.size
}
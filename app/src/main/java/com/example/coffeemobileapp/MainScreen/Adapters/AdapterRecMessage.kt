package com.example.coffeemobileapp.MainScreen.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.coffeemobileapp.common.Models.ModelMessage
import com.example.coffeemobileapp.R

class AdapterRecMessage(val listMessage: List<ModelMessage>): RecyclerView.Adapter<AdapterRecMessage.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.title_message)
        val message = itemView.findViewById<TextView>(R.id.message)
        val src = itemView.findViewById<ImageView>(R.id.src)
        val send = itemView.findViewById<CardView>(R.id.card_message)
        val text_send = itemView.findViewById<TextView>(R.id.text_send)
        val data = itemView.findViewById<TextView>(R.id.data_message)
        val name = itemView.findViewById<TextView>(R.id.name_profile)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_message, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = listMessage[position].title
        holder.message.text = listMessage[position].message
        holder.data.text = listMessage[position].data
        holder.text_send.text = "Message ${listMessage[position].user.name}"
        holder.name.text = listMessage[position].user.name
        holder.send.setOnClickListener {

        }
    }

    override fun getItemCount(): Int = listMessage.size
}
package com.example.coffeemobileapp.MainScreen.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.coffeemobileapp.MainScreen.onCLickButtonChat
import com.example.coffeemobileapp.R
import kotlinx.android.synthetic.main.fragment_message.*

class FragmentResources: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_resources, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        button_chat.setOnClickListener {
            onCLickButtonChat.onClick(it)
        }
    }
}
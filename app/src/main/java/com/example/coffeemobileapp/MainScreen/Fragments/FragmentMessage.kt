package com.example.coffeemobileapp.MainScreen.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.coffeemobileapp.MainScreen.Adapters.AdapterRecMessage
import com.example.coffeemobileapp.MainScreen.onCLickButtonChat
import com.example.coffeemobileapp.R
import com.example.coffeemobileapp.common.Info
import kotlinx.android.synthetic.main.fragment_message.*

class FragmentMessage: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_message, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        button_chat.setOnClickListener {
            onCLickButtonChat.onClick(it)
        }

        button_chat_second.setOnClickListener {
            onCLickButtonChat.onClick(it)
        }

        rec_message.apply{
            adapter = AdapterRecMessage(Info.listMessage)
            layoutManager = LinearLayoutManager(requireContext())
        }


    }
}
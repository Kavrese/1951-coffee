package com.example.coffeemobileapp.MainScreen.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.coffeemobileapp.MainScreen.Fragments.FragmentClasses
import com.example.coffeemobileapp.MainScreen.Fragments.FragmentJobs
import com.example.coffeemobileapp.MainScreen.Fragments.FragmentMessage
import com.example.coffeemobileapp.MainScreen.Fragments.FragmentResources

class AdapterFragmentPager(fr: FragmentActivity): FragmentStateAdapter(fr) {
    override fun getItemCount(): Int = 4

    override fun createFragment(position: Int): Fragment {
        return arrayListOf(FragmentMessage(), FragmentJobs(), FragmentClasses(), FragmentResources())[position]
    }

}
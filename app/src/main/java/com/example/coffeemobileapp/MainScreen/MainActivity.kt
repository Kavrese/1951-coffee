package com.example.coffeemobileapp.MainScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import com.example.coffeemobileapp.MainScreen.Adapters.AdapterFragmentPager
import com.example.coffeemobileapp.MainScreen.Adapters.AdapterRecTypeChat
import com.example.coffeemobileapp.R
import com.example.coffeemobileapp.common.Info
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_chat.*

lateinit var onCLickButtonChat: View.OnClickListener

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initMain()
        createChatClick()
    }

    private fun createChatClick() {
        onCLickButtonChat = View.OnClickListener {
            inc_chat.visibility = View.VISIBLE
        }
        close_chat.setOnClickListener {
            inc_chat.visibility = View.GONE
        }
        rec_chat.apply {
            adapter = AdapterRecTypeChat(Info.listUsers)
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    private fun initMain(){
        pager.adapter = AdapterFragmentPager(this)
        bottom_nav_view.setOnItemSelectedListener {
            pager.setCurrentItem(when(it.itemId){
                R.id.item_1 -> 0
                R.id.item_2 -> 1
                R.id.item_3 -> 2
                R.id.item_4 -> 3
                else -> 0
            }, true)
            return@setOnItemSelectedListener true
        }

        pager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                bottom_nav_view.selectedItemId = arrayListOf(R.id.item_1, R.id.item_2, R.id.item_3, R.id.item_4)[position]
            }
        })
    }
}
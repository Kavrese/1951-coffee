package com.example.coffeemobileapp.MainScreen.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.coffeemobileapp.common.Models.ModelMessage
import com.example.coffeemobileapp.R
import com.example.coffeemobileapp.common.Models.ModelUser

class AdapterRecTypeChat(var listUser: List<ModelUser>, val listType: List<String> = listUser.map{it.type}.toSet().toList()): RecyclerView.Adapter<AdapterRecTypeChat.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val type = itemView.findViewById<TextView>(R.id.type)
        val rec_chat = itemView.findViewById<RecyclerView>(R.id.rec_user)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_type_chat, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.type.text = listUser[position].type
        holder.rec_chat.apply {
            layoutManager = LinearLayoutManager(holder.itemView.context)
            adapter = AdapterRecUser(listUser.filter { it.type == listType[position]})
        }
    }

    override fun getItemCount(): Int = listType.size
}